package hellocucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
// import org.junit.platform.suite.api.ConfigurationParameter;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/hellocucumber", plugin = {"pretty", "hellocucumber.reporters.LippiaReporter:"}, tags = "@Example")
public class RunCucumberTest {
    // @ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty, message:target/cucumber-messages.ndjson")
}
