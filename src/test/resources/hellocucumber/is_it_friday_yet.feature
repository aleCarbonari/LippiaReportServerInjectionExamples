@Example @Cucumber5
Feature: Sunday is ...

  @Success
  Scenario Outline: Sunday isn't <day>
    Given today is not "<day>"

    @Table1
    Examples:
      | day       |
      | Wednesday |
      | Saturday  |

    @Table2
    Examples:
      | day     |
      | Tuesday |
      | Monday  |

  @Success
  Scenario: Sunday isn't Friday
    Given today is Sunday

  @Success
  Scenario: Sunday isn't Wednesday
    Given today is not Sunday